package br.com.treinaweb.twprojetos.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Data;

@Entity
@Data
public class Funcionario extends Pessoa {

  @Column(name = "data_admissao", nullable = false)
  @DateTimeFormat(iso = ISO.DATE)
  private LocalDate dataAdmissao;

  @Column(name = "data_demissao")
  @DateTimeFormat(iso = ISO.DATE)
  private LocalDate dataDemissao;
}
